package com.example.apitechu;

import com.example.apitechu.models.ProductModel;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

@SpringBootApplication
@RestController
public class ApitechuApplication {


	public static ArrayList<ProductModel> productModel;

	public static void main(String[] args) {

		SpringApplication.run(ApitechuApplication.class, args);
		ApitechuApplication.productModel = ApitechuApplication.getTestData();

	}



	private static ArrayList<ProductModel> getTestData(){

		ArrayList<ProductModel> listProductModel = new ArrayList<>();

		listProductModel.add(
			new ProductModel(
					"1",
					"producto 1",
					10
			)
		);

		listProductModel.add(
				new ProductModel(
						"2",
						"producto 2",
						100
				)
		);

		listProductModel.add(
				new ProductModel(
						"3",
						"producto 3",
						1000
				)
		);

		return listProductModel;
	}

}
