package com.example.apitechu.controllers;

import com.example.apitechu.ApitechuApplication;
import com.example.apitechu.models.ProductModel;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Map;

@RestController
public class ProductController {

    static final String APIBaseUrl = "/apitechu/v1/";

    @GetMapping(APIBaseUrl + "products")
    public ArrayList<ProductModel> getProducts(){

        System.out.println("getproducts");

        return ApitechuApplication.productModel;
    }

    @GetMapping(APIBaseUrl + "products/{id}")
    public ProductModel getProductById(@PathVariable String id){

        System.out.println("getProductById");
        System.out.println("id es " + id);

        ProductModel result = new ProductModel();

        for(ProductModel product: ApitechuApplication.productModel){
            if(product.getId().equalsIgnoreCase(id)){
                result = product;
            }
        }

        return result;
    }

    @PostMapping(APIBaseUrl + "products")
    public ProductModel createproduct(@RequestBody ProductModel newProduct){

        System.out.println("createProduct");
        System.out.println("Id" + newProduct.getId());
        System.out.println("Descripcion" + newProduct.getDesc());
        System.out.println("Precio" + newProduct.getPrice());

        ApitechuApplication.productModel.add(newProduct);

        return newProduct;
    }

    @PutMapping(APIBaseUrl + "products/{id}")
    public ProductModel updateProduct(
            @RequestBody ProductModel newProduct,
            @PathVariable String id
    ){
        System.out.print("updateProducts");
        System.out.println("la id del producto a actualizar en parametro de URL " +  id);
        System.out.println("id a actualizar " + newProduct.getId());
        System.out.println("desc a actualizar " + newProduct.getDesc());
        System.out.println("precio a actualizar " + newProduct.getPrice());

        for(ProductModel product: ApitechuApplication.productModel){
            if(product.getId().equalsIgnoreCase(id)){
               product.setId(newProduct.getId());
               product.setDesc(newProduct.getDesc());
               product.setPrice(newProduct.getPrice());
            }
        }

        return newProduct;
    }

    @DeleteMapping(APIBaseUrl + "products/{id}")
    public ProductModel deleteProduct(@PathVariable String id){

        System.out.println("deleteProduct");
        System.out.println("El artucullo a eliminar" + id);

        ProductModel productModel = new ProductModel();
        boolean foundProduct = false;

        for(ProductModel product: ApitechuApplication.productModel){
            if(product.getId().equalsIgnoreCase(id)){
                System.out.println("Producto encontrado");
                foundProduct = true;
                productModel = product;
            }
        }

        if(foundProduct){
            System.out.println();
            ApitechuApplication.productModel.remove(productModel);
        }

        return productModel;
    }

    @PatchMapping(APIBaseUrl + "products/{id}")
    public ProductModel updatePatch(
            @PathVariable String id,
            @RequestBody ProductModel productModel
    ){

        System.out.println("updatePatchProduct");
        System.out.println("id a actualizar " + id);
        System.out.println("desc a actualizar " + productModel.getDesc());
        System.out.println("price a actulizar " + productModel.getPrice());

        ProductModel baseProductModel = new ProductModel();

        for(ProductModel product: ApitechuApplication.productModel){
            if(product.getId().equalsIgnoreCase(id)){
                System.out.println("Producto encontrado");
                String desc = null == productModel.getDesc() ? product.getDesc() : productModel.getDesc();
                float price = 0 == productModel.getPrice()  ? product.getPrice() : productModel.getPrice();
                product.setDesc(desc);
                product.setPrice(price);
                baseProductModel = product;
            }
        }

        return baseProductModel;
    }

}
